package crius_plugin_quotes

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/gempir/go-twitch-irc/v2"
	"github.com/jmoiron/sqlx"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/GuildCrius"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	goleshchat "gitlab.com/ponkey364/golesh-chat"
	"math/rand"
	"time"
)

var PluginInfo = &cc.PluginJSON{
	Name:               "Quotes",
	License:            "BSD-3-Clause",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/crius-plugin-quotes/",
	Description:        "Store and get quotes",
	SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
	Commands: []*cc.PJCommand{
		{
			Name:               "Get Quote",
			Help:               "Get either the quote with the given ID or a random quote (if no ID is provided)",
			HandlerName:        "quote",
			Activator:          "quote",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
		{
			Name:               "Add Quote",
			Help:               "Add a new quote to the database for this realm. Usage `addquote [quote]`",
			HandlerName:        "addquote",
			Activator:          "addquote",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
		{
			Name:               "Get Quote Count",
			Help:               "Get the amount of quotes for this realm",
			HandlerName:        "quotecount",
			Activator:          "quotecount",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
		{
			Name:               "Remove Quote",
			Help:               "Remove a quote. Usage: `rmquote [id]`",
			HandlerName:        "rmquote",
			Activator:          "rmquote",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh | cc.PlatformDiscord,
		},
	},
}

type quotes struct {
	db *sqlx.DB
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	// get stuff we need from ctx
	s := CriusUtils.GetSettings(ctx)

	db := s.GetDB()

	// run the table creation script from db.go
	// create a transaction
	tx := db.MustBegin()

	// run the script
	_, err := tx.Exec(tableCreateScript)
	if err != nil {
		return nil, err
	}

	// done
	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	q := &quotes{
		db: db,
	}

	return map[string]cc.CommandHandler{
		"quote":      q.GetQuote,
		"addquote":   q.AddQuote,
		"quotecount": q.GetQuoteCount,
		"rmquote":    q.RemoveQuote,
	}, nil
}

func (q *quotes) GetQuote(m *cc.MessageContext, args []string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID

	var id string

	if len(args) == 0 {
		// ENGAGE THE RNG!
		seed := rand.NewSource(time.Now().Unix())
		rng := rand.New(seed)

		// get a list of the IDs related to this realm & platfrom
		var ids []string
		err := q.db.Select(&ids, "SELECT quote_id FROM quotes__quotes WHERE platform=$1 AND realm_id=$2",
			platform.ToString(), realmID)
		if err != nil {
			return err
		}

		i := rng.Intn(len(ids))
		id = ids[i]
	} else {
		id = args[0]
	}

	var quote string
	err := q.db.Get(&quote, "SELECT quote FROM quotes__quotes WHERE quote_id=$1 AND platform=$2 AND realm_id=$3",
		id, platform.ToString(), realmID)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if errors.Is(err, sql.ErrNoRows) {
		m.Send("There is no quote with that ID")
		return nil
	}

	m.Send(fmt.Sprintf("Quote %s: %s", id, quote))

	return nil
}

func (q *quotes) AddQuote(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("you must specify a quote. Usage: `Usage: addquote [quote]`")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID

	switch platform {
	case cc.PlatformDiscord:
		{
			isAdmin, err := GuildCrius.GetPermissions(m.Context).IsServerAdmin(m.Author.ID, m.RealmID)
			if err != nil {
				return err
			}

			if !isAdmin {
				m.Send("Only server admins can add new quotes.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)

			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("Only the streamer can add new quotes.")
				return nil
			}
		}
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitch.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("Only the streamer or moderators can add new quotes.")
				return nil
			}
		}
	}

	_, err := q.db.Exec("INSERT INTO quotes__quotes (realm_id, platform, quote) VALUES ($1, $2, $3)",
		realmID, platform.ToString(), args[0])
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("Added quote \"%s\"", args[0]))

	return nil
}

func (q *quotes) GetQuoteCount(m *cc.MessageContext, _ []string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID

	var quoteCount int
	err := q.db.Get(&quoteCount, "SELECT COUNT(*) FROM quotes__quotes WHERE platform=$1 AND realm_id=$2",
		platform.ToString(), realmID)
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("There are %d quotes in this realm", quoteCount))

	return nil
}

func (q *quotes) RemoveQuote(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("you must specify a quote ID. Usage: `rmquote [id]`")
		return nil
	}

	platform := CriusUtils.GetPlatform(m.Context)

	realmID := m.RealmID

	switch platform {
	case cc.PlatformDiscord:
		{
			isAdmin, err := GuildCrius.GetPermissions(m.Context).IsServerAdmin(m.Author.ID, m.RealmID)
			if err != nil {
				return err
			}

			if !isAdmin {
				m.Send("Only server admins can remove quotes.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)

			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("Only the streamer can remove quotes.")
				return nil
			}
		}
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitch.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("Only the streamer or moderators can remove quotes.")
				return nil
			}
		}
	}

	_, err := q.db.Exec("DELETE FROM quotes__quotes WHERE realm_id=$1 AND platform=$2 AND quote_id=$3",
		realmID, platform.ToString(), args[0])
	if err != nil {
		return err
	}

	m.Send("Removed quote " + args[0])

	return nil
}
